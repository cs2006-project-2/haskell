module REPL where

import Expr
import Parsing
import Control.Monad.Trans.Class
import System.IO
import System.Console.Haskeline
import Data.Maybe

data State = State { vars :: [(Name, Value)] }

initState :: REPL.State
initState = State []

-- Given a variable name and a value, return a new set of variables with that name and value added.
-- If it already exists, remove the old value, replacing it with the new one.
updateVars :: Name -> Value -> [(Name, Value)] -> [(Name, Value)]
updateVars name val vars 
                         | any ((==name).fst) vars     = map (\(x,y) -> if x == name then (name, val) else (x,y)) vars
                         | otherwise                   = vars ++ [(name, val)]

-- Return a new set of variables with the given name removed
dropVar :: Name -> [(Name, Value)] -> [(Name, Value)]
dropVar name = filter ((==name).fst) 

-- attempts to print value of a given variable name - if variable does not exist just print the name 
printer :: Name -> [(Name, Value)] -> IO ()
printer name vars | any ((==name).fst) vars             = print (snd (head (filter ((==name).fst) vars)))
                  | otherwise                           = print name

-- Processing of Commands
process :: REPL.State -> Command -> IO ()

-- Processing of the 'Set' command - evaluates 'e' and applies it to the variable 'var'
process st (Set var e) 
     = do let st' | isNothing (eval (vars st) e)                      = st
                  | otherwise                                         = st { vars = updateVars var (fromJust ( eval (vars st) e)) (vars st)}
          repl st'

-- Processing of the 'Print' command - outputs e to the user
process st (Print e) 
     = do case fromJust (eval (vars st) e) of
               IntVal i -> print i
               DblVal d -> print d
               StrVal s -> printer s (vars st)
          repl st 

-- Processing of the 'Input' command - takes the next line as input from user and assigns it to variable var
process st (Input var)
     = do inp <- getLine 
          let st' = st { vars = updateVars var (fromJust ( eval (vars st) (Val (StrVal inp)))) (vars st)}
          repl st'

-- Processing of the 'Track' command - displays variable name and values from State - vars
process st Track 
     = do print (vars st)
          repl st

-- Processing of the 'Clear' command - clears the variables, restores initial state 
process st Clear
     = do repl initState

-- Processing of the 'Quit' command - outputs goodbye and ends the Read Eval Print loop
process st Quit
     = do putStr ("Goodbye!" ++ "\n")
          return ()

-- Read, Eval, Print Loop
-- This reads and parses the input using the pCommand parser, and calls
-- 'process' to process the command.
-- 'process' will call 'repl' when done, so the system loops.

repl :: State -> IO ()
repl st = do putStr ("> ")
             inp <- getLine
             case parse pCommand inp of
                  [(cmd, "")] -> process st cmd
                  _ -> do putStrLn "Parse error"
                          repl st
