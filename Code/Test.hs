{-# LANGUAGE TemplateHaskell #-}
module Main where

import Test.QuickCheck
import Test.QuickCheck.All()
import Parsing()
import Expr

-- simple test to see if quickcheck is working
prop_reverse :: [Int] -> Bool
prop_reverse xs = reverse (reverse xs) == xs

-- tests to see if toInt works
prop_toInt :: Int -> Bool
prop_toInt a = a == value' where
                        a' = Val (StrVal (show a))
                        value = eval [("a", IntVal 0)] (ToInt a')
                        value' = maybe 0 getInt value

-- tests to see if toInt works
prop_toDouble :: Double -> Bool
prop_toDouble a = a == value' where
                        a' = Val (StrVal (show a))
                        value = eval [("a", DblVal 0)] (ToDouble a')
                        value' = maybe 0 getDbl value

-- tests to see if toStr works
prop_toStr :: Int -> Bool
prop_toStr a = show a == value' where
                        a' = Val (IntVal a)
                        value = eval [("a", StrVal "0")] (ToString a')
                        value' = maybe "" getStr value

-- tests to see if the addition function works
prop_addition :: Int -> Int -> Bool
prop_addition a b = do 
    a + b  == value'' where 
                     a' = Val (IntVal a)
                     b' = Val (IntVal b)
                     expr' = Add a' b'
                    --  contents of list [] are irrelevant, must be put in to follow syntax of code 
                     value' = eval [("a", IntVal 0)] expr'
                     value'' = maybe 0 getInt value'

-- tests to see if the subtraction function works
prop_subtraction :: Int -> Int -> Bool
prop_subtraction a b = do 
    a - b  == value'' where 
                     a' = Val (IntVal a)
                     b' = Val (IntVal b)
                     expr' = Sub a' b'
                    --  contents of list [] are irrelevant, must be put in to follow syntax of code 
                     value' = eval [("a", IntVal 0)] expr'
                     value'' = maybe 0 getInt value'

-- tests to see if the multiplication function works
prop_multiplication :: Int -> Int -> Bool
prop_multiplication a b = do 
    a * b  == value'' where 
                     a' = Val (IntVal a)
                     b' = Val (IntVal b)
                     expr' = Mul a' b'
                    --  contents of list [] are irrelevant, must be put in to follow syntax of code 
                     value' = eval [("a", IntVal 0)] expr'
                     value'' = maybe 0 getInt value'

-- tests to see if the division function works, Modifier for nonzero denominator to prevent dividing by zero
prop_division ::  Double -> NonZero Double -> Bool
prop_division a (NonZero b) = do 
    a / b  == value'' where 
                     a' = Val (DblVal a)
                     b' = Val (DblVal b)
                     expr' = Div a' b'
                    --  contents of list [] are irrelevant, must be put in to follow syntax of code 
                     value' = eval [("a", DblVal 0)] expr'
                     value'' = maybe 0 getDbl value'

-- tests to see if the concatenation function works
prop_string_concat ::  String -> String -> Bool
prop_string_concat a b = do 
    a ++ b  == value'' where 
                     a' = Val (StrVal a)
                     b' = Val (StrVal b)
                     expr' = Concat a' b'
                    --  contents of list [] are irrelevant, must be put in to follow syntax of code 
                     value' = eval [("a", StrVal "0")] expr'
                     value'' = maybe "" getStr value'


-- tests to see if the modulo function works, Modifier is positive only because prelude isn't capable of modulo of negative numbers
prop_modulo ::  Positive Int ->  Positive Int -> Bool
prop_modulo (Positive a) (Positive b) = do rem a b  == value'' where 
                               a' = Val (IntVal a)
                               b' = Val (IntVal b)
                               expr' = Mod b' a'
                               --  contents of list [] are irrelevant, must be put in to follow syntax of code 
                               value' = eval [("a", IntVal 0)] expr'
                               value'' = maybe 0 getInt value'

-- tests to see if the power function works
prop_power :: Int -> Positive Int -> Bool
prop_power a (Positive b) = do 
    a ^ b  == value'' where 
                     a' = Val (IntVal a)
                     b' = Val (IntVal b)
                     expr' = Power a' b'
                    --  contents of list [] are irrelevant, must be put in to follow syntax of code 
                     value' = eval [("a", IntVal 0)] expr'
                     value'' = maybe 0 getInt value'

-- tests to see if the factorial function works
prop_factorial :: Positive Int -> Bool
prop_factorial (Positive a) = do 
    fac a  == value'' where 
                     a' = Val (IntVal a)
                     expr' = Factorial a'
                    --  contents of list [] are irrelevant, must be put in to follow syntax of code 
                     value' = eval [("a", IntVal 0)] expr'
                     value'' = maybe 0 getInt value'
                     fac 0 = 1
                     fac a = a * fac (a - 1)             


return []
main :: IO Bool
main = $quickCheckAll
