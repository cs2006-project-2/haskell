module Expr where

import Parsing

import Text.Read

import Data.Maybe

type Name = String

data Value = IntVal Int | DblVal Double | StrVal String
  deriving Show

-- These are the expressions
data Expr = Add Expr Expr
          | Sub Expr Expr
          | Mul Expr Expr
          | Div Expr Expr
          | Mod Expr Expr
          | Power Expr Expr
          | Concat Expr Expr
          | Factorial Expr 
          | ToString Expr
          | ToDouble Expr
          | ToInt Expr
          | Val Value
          | Var Name
  deriving Show

-- These are the REPL commands
data Command = Set Name Expr  -- assign an expression to a variable name
             | Print Expr     -- evaluate an expression and print the result
             | Input Name     -- take next line as String input and assign it to a variable name
             | Clear          -- removes all variables - returns back to initial state
             | Track          -- Track the system State - current variables and their values
             | Quit           -- exit the program and return to ghci prompt
  deriving Show

-- EVALUATOR

eval :: [(Name, Value)] ->    -- Variable name to value mapping
        Expr ->               -- The expression to be evaluated
        Maybe Value           -- Result (if no errors such as missing variables)

eval vars (Val x)           = Just x             -- for values, just give the value directly

eval vars (Var x)           = getVar x vars      -- for variables, get the value from the variable        

eval vars (Add x y)         = do                 -- Addition (x + y)
                          a <- eval vars x
                          b <- eval vars y
                          Just (IntVal (round (getDbl a + getDbl b)))

eval vars (Sub x y)        = do                  -- Subtraction (x - y)
                         a <- eval vars x
                         b <- eval vars y
                         Just (IntVal (round (getDbl a - getDbl b)))

eval vars (Mul x y)        = do                  -- Multiplication (x * y)
                         a <- eval vars x
                         b <- eval vars y
                         Just (IntVal (round (getDbl a * getDbl b)))

eval vars (Div x y)       = do                   -- Integer Division (x / y)
                        a <- eval vars x
                        b <- eval vars y
                        Just (DblVal (getDbl a / getDbl b))

eval vars (Mod x y)       = do                   -- Get Modulo (x mod y)
                        a <- eval vars x
                        b <- eval vars y
                        Just (IntVal (mod (getInt b) (getInt a)))

eval vars (Power x y)     = do                   -- Get Modulo (x mod y)
                        a <- eval vars x
                        b <- eval vars y
                        Just (IntVal  (getInt a ^ getInt b))

eval vars (Concat x y)    = do                   -- Concatinate 2 values into 1 string
                      a <- eval vars x
                      b <- eval vars y
                      Just (StrVal (getStr a ++ getStr b))

eval vars (Factorial x)   = do                   -- Get Factorial of x (x * (x-1) * ... 1)
                        a <- eval vars x
                        Just (IntVal (fac (getInt a)))

                          where fac 0 = 1
                                fac a = a * fac (a - 1)                      

eval vars (ToInt x)       = do                   -- Smart String to Int conversion
                      a <- eval vars x
                      Just (IntVal (getInt a))

eval vars (ToString x)    = do                   -- Int to String conversion
                      a <- eval vars x
                      Just (StrVal (getStr a))

eval vars (ToDouble x)    = do
                     a <- eval vars x
                     Just (DblVal (getDbl a))

-- Retrieves Int from value, works for int and str, if str not compatible, results in '0', rounds Doubles
getInt :: Value -> Int
getInt (IntVal x) = x
getInt (DblVal x) = round x
getInt (StrVal x) = if isNothing (readMaybe x :: Maybe Int) then 0 else read x

getDbl :: Value -> Double
getDbl (IntVal x) = fromIntegral x :: Double
getDbl (DblVal x) = x
getDbl (StrVal x) = if isNothing (readMaybe x :: Maybe Double) then 0 else read x

-- Retrieves String from value, works for int and str
getStr :: Value -> [Char]
getStr (IntVal x) = show x
getStr (DblVal x) = show x
getStr (StrVal x) = x

-- Retrieves value from variable, if the variable doesn't exist - returns the input name as a StrVal
getVar :: Name -> [(Name, Value)] -> Maybe Value
getVar x vars = case filter of
                  [y] -> Just y 
                  []  -> eval vars (Val(StrVal x))
                where filter = [val | (str, val) <- vars, str == x]

-- PARSER

-- parse command
pCommand :: Parser Command
pCommand = do t <- token (many letter)
              char '='                           -- > `variable_name` = `something`
              Set t <$> pExpr

            ||| do string "print"                -- > print `something`
                   space
                   Print <$> pExpr

            ||| do string "input"                -- > input `variable_name`
                   space
                   Input <$> many letter   

            ||| do string "clear"                -- > clear
                   space
                   return Clear

            ||| do string "track"                -- > track
                   space
                   return Track

            ||| do string "quit"                 -- > quit
                   space
                   return Quit

-- parse expression
pExpr :: Parser Expr
pExpr = do t <- token pTerm
           do char '+'                           -- `value` + `value`
              Add t <$> pExpr  

            ||| do char '-'                      -- `value` - `value`
                   Sub t <$> pExpr  
                   ||| return t

-- parse factor
pFactor :: Parser Expr
pFactor = do string "fact"                       -- fact `value`
             Factorial <$> pExpr
  
           ||| do string "toInt"                 -- toInt `value` 
                  space
                  ToInt <$> pExpr

           ||| do string "toString"              -- toString `value`
                  space
                  ToString <$> pExpr

           ||| do string "toDouble"              -- toDouble `value`
                  space
                  ToDouble <$> pExpr
 
           ||| do char '('                       -- For expressions within brackets  
                  e <- token pExpr
                  char ')'
                  return e

           ||| do char '"'                       -- For Strings within speech marks
                  i <- many pChar
                  char '"'
                  return (Val (StrVal i)) 

           ||| do j <- letter                    -- For Variable names
                  k <- many letter
                  return (Var (j : k))  

           ||| do n <- pNumeric
                  d <- many (token digit)        -- For Decimal Numbers  
                  x <- pDec
                  y <- many (token digit)
                  return (Val( DblVal (read ((n : d) ++ (x : y)) :: Double))) -- combines everything inti a single String, then reads it as a Double             

           ||| do n <- pNumeric
                  d <- many (token digit)        -- For Integer Numbers  
                  return (Val( IntVal (read (n : d))))    

-- parse term
pTerm :: Parser Expr
pTerm = do f <- token pFactor
           do char '*'                           -- `value` * `value`
              Mul f <$> pExpr

            ||| do char '/'                      -- `value` / `value`
                   Div f <$> pExpr

            ||| do char '^'                      -- `value` ^ `value`
                   Power f <$> pExpr

            ||| do string "++"                   -- `value` ++ `value`
                   Concat f <$> pExpr 

            ||| do string "mod"                  -- `value` mod `value`
                   Mod f <$> pExpr      

                 ||| return f

-- parse char for "multi word strings"
pChar :: Parser Char   
pChar = do char ' ' 
           return ' ' 

         ||| do char '.'
                return '.' 

         ||| do alphanum 

-- parse char for first elements of a number - can either be '-' indicating a negative number, or an actual digit.
pNumeric :: Parser Char 
pNumeric = do char '-'
              return '-'
           ||| do digit

-- parse char for decimal points
pDec :: Parser Char 
pDec = do char '.'
          return '.'